package com.buddy.BuddyRide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuddyRideApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuddyRideApplication.class, args);
	}
}
